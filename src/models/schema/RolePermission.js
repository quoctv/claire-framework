let RolePermission;
const s = (builder, Sequelize) => {
	RolePermission = builder.define('RolePermission', {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		roleId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		permissionId: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		permissionSettings: {
			type: Sequelize.STRING,
		},
	}, {});
	RolePermission.associate = (models) => {
		RolePermission.belongsTo(models['Role']); //-- user instance then call the promise user.getRole()
	};
	return RolePermission;
};

export {RolePermission, s};
