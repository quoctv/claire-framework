const AUTH_METHODS = {
  AUTH_PASSWORD: 'password',
  AUTH_OAUTH: 'oauth',
};

let Auth;
const s = (builder, Sequelize) => {
	Auth = builder.define('Auth', {
		authMethod: {
			type: Sequelize.STRING,
			primaryKey: true,
			allowNull: false,
			validate: {
				isIn: [Object.keys(AUTH_METHODS).map(key => AUTH_METHODS[key])]
			}
		},
		authInfo1: {
			type: Sequelize.STRING
		},
		authInfo2: {
			type: Sequelize.STRING
		},
		userId: {
			type: Sequelize.INTEGER,
			allowNull: false,
			primaryKey: true,
		},
	}, {});
	Auth.associate = (models) => {
		Auth.belongsTo(models['User']); //-- auth instance then call the promise auth.getUser()
	};
	return Auth;
};

export {Auth, s, AUTH_METHODS};
