let OAuthApp;
const m = (builder, Mongoose)=>{
	OAuthApp = builder.model('OAuthApp', new Mongoose.Schema({
		userId: {type: Number, required: true},
		name: {type: String, require: true},
		appKey: {type: String, required: true, select: false},
		callbackUrls: {type: Array, default: []},
		createdAt: {type: Date, required: true, default: Date.now},
	}));
};

export {OAuthApp, m};
