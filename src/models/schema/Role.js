let Role;
const s = (builder, Sequelize) => {
	Role = builder.define('Role', {
		name: {
			allowNull: false,
			type: Sequelize.STRING
		},
		super_user: {
			type: Sequelize.BOOLEAN
		},
		editable: {
			type: Sequelize.BOOLEAN
		},
	}, {});
	Role.associate = (models) => {
	};
	return Role;
};

export {Role, s};
