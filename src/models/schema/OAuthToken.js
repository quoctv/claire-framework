const GRANT_TYPES = {
  PASSWORD: 'password',
  OAUTH: 'oauth',
  REFRESH_TOKEN: 'refresh',
  AUTH_CODE: 'code',
  API_KEY: 'api_key',
};

const ACCESS_SCOPES = {
  FULL_SCOPE: 'full_scope',
  BASIC_SCOPE: 'basic_scope',
  RESTRICTED_SCOPE: 'restricted_scope',
  APP_SCOPE: 'app_scope',
};

let OAuthToken;
const m = (builder, Mongoose) =>{
	OAuthToken = builder.model('OAuthToken', new Mongoose.Schema({
		grantType: {type: String, required: true},
		owner: {type: Number, required: true},
		delegate: {type: Mongoose.Schema.ObjectId, ref: 'OAuthApp'},
		scope: {type: String, require: true},
	}));
};

export {OAuthToken, m, GRANT_TYPES, ACCESS_SCOPES};
