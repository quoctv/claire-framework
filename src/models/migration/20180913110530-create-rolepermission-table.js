'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('RolePermissions', {
      'id': {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      'roleId': {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Roles',
          key: 'id'
        }
      },
      'permissionId': {
        type: Sequelize.STRING,
        allowNull: false,
      },
      'permissionSettings': {
        type: Sequelize.STRING,
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('RolePermissions', {});
  }
};
