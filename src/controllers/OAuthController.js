import jwt from 'jsonwebtoken';
import {authenticated} from '../middleware/policies';
import {validGrantType, validObjectId, validScope, valueRequired} from '../middleware/validators';
import {isValidObjectId} from '../utils/validation';
import {ACCESS_SCOPES, GRANT_TYPES, OAuthToken} from '../models/schema/OAuthToken'
import {errors, jsonError, jsonSuccess, logger} from '../utils/system';
import {UserService} from '../services/UserService';
import {OAuthService} from '../services/OAuthService';
import {OAuthApp} from '../models/schema/OAuthApp';
import {OAuthRequest} from '../models/schema/OAuthRequest';

const OAuthController = require('express').Router();

OAuthController.route('/token')
  .post([
    valueRequired({attributes: ["grantType", "grantData"]}),
    validGrantType({attributes: ["grantType"]}),
    validScope({attributes: ["scope"]}),
  ], (req, res) => {
    (async () => {
      let tokenData = {};
      switch (req.body['grantType']) {
        case GRANT_TYPES.PASSWORD:
        case GRANT_TYPES.OAUTH: {
          let loginData = {
            authMethod: req.body['grantType'],
            authData: req.body['grantData']
          };
          let loginResult = await UserService.login(loginData);
          if (!loginResult.success)
            return loginResult;

          tokenData.grantType = req.body.grantType;
          tokenData.owner = loginResult.result._id;
          tokenData.scope = req.body.scope || ACCESS_SCOPES.BASIC_SCOPE;
          break;
        }

        case GRANT_TYPES.AUTH_CODE: {

          let [code, appId, appKey, redirectUrl] = req.body['grantData'];
          code = isValidObjectId(code);
          appId = isValidObjectId(appId);

          let request = await OAuthRequest.findOne({
            _id: code,
            appId: appId,
            accepted: true,
            callbackUrl: redirectUrl
          });
          let app = await OAuthApp.findById(appId).select('appKey');

          if (!app || !request)
            return jsonError(errors.NOT_FOUND_ERROR);
          if (app.appKey !== appKey)
            return jsonError(errors.OAUTH_APP_KEY_MISMATCHED);
          if (request.expiredAt.getTime() < Date.now())
            return jsonError(errors.REQUEST_EXPIRED);

          //-- issue the token
          request.expiredAt = Date.now();
          await request.save();

          tokenData.grantType = req.body.grantType;
          tokenData.delegate = app._id;
          tokenData.owner = request.userId;
          tokenData.scope = request.scope;

          break;
        }

        case GRANT_TYPES.REFRESH_TOKEN: {
          let [refreshToken] = req.body['grantData'];
          let verifyResult = await new Promise((resolve) => {
            jwt.verify(refreshToken, getEnv('JWT_SECRET'), (err, decoded) => {
              if (err) {
                logger.error('POST /oauth/token', err);
                return resolve(jsonError(errors.INVALID_TOKEN));
              }
              return resolve(jsonSuccess(decoded));
            });
          });

          if (!verifyResult.success)
            return verifyResult;

          //-- find the token
          let token = await OAuthToken.findById(verifyResult.result.tokenId);
          if (!token)
            return jsonError(errors.TOKEN_REVOKED_ERROR);

          tokenData.grantType = GRANT_TYPES.REFRESH_TOKEN;
          tokenData.owner = token.owner;
          tokenData.scope = token.scope;
          tokenData.delegate = token.delegate;

          await token.remove();
          break;
        }

        default: {
          return jsonError(errors.INVALID_GRANT_TYPE);
        }
      }
      return await OAuthService.getAccessToken({tokenData});
    })()
      .then((result) => {
        res.json(result);
      })
      .catch((err) => {
        logger.error('POST /oauth/token', err);
        res.json(jsonError(errors.SYSTEM_ERROR));
      });
  });

OAuthController.route('/app')
  .get([
    authenticated(),
  ], (req, res) => {
    let searchObject = {userId: req.principal.owner};
    if (req.body.ids && Array.isArray(req.body.ids))
      Object.assign(searchObject, {_id: {$in: req.body.ids}});
    OAuthService.getOAuthApps({filter: searchObject})
      .then((result) => {
        res.json(result);
      })
      .catch((err) => {
        logger.error('GET /oauth/app', err);
        res.json(jsonError(errors.SYSTEM_ERROR));
      });
  })
  .post([
    authenticated(),
    valueRequired({attributes: ["name", "callbackUrls"]}),
  ], (req, res) => {
    OAuthService.createOAuthApp(req.body)
      .then((result) => {
        res.json(result);
      })
      .catch((err) => {
        logger.error('POST /oauth/app', err);
        res.json(jsonError(errors.SYSTEM_ERROR));
      });
  })
  .delete([
    authenticated(),
    valueRequired({attributes: ["id"]}),
    validObjectId({attributes: ["id"]}),
  ], (req, res) => {
    OAuthService.removeOAuthApp(req.body)
      .then((result) => {
        res.json(result);
      })
      .catch((err) => {
        logger.error('DELETE /oauth/app', err);
        res.json(jsonError(errors.SYSTEM_ERROR));
      });
  });

OAuthController.route('/generate_request')
  .post([
    authenticated(),
    valueRequired({attributes: ["appId", "state", "scope", "redirectUrl"]}),
    validObjectId({attributes: ["appId"]}),
    validScope({attributes: ["scope"]}),
  ], (req, res) => {
    OAuthService.createOAuthRequest(req.body)
      .then((result) => {
        res.json(result);
      })
      .catch((err) => {
        logger.error('POST /oauth/generate_request', err);
        res.json(jsonError(errors.SYSTEM_ERROR));
      });
  });

OAuthController.route('/grant_request')
  .post([
    authenticated(),
    valueRequired({attributes: ["id", "decision"]}),
    validObjectId({attributes: ["id"]}),
  ], (req, res) => {
    OAuthService.grantOAuthRequest(req.body)
      .then((result) => {
        res.json(result);
      })
      .catch((err) => {
        logger.error('POST /oauth/grant_request', err);
        res.json(jsonError(errors.SYSTEM_ERROR));
      });
  });

export {OAuthController};
